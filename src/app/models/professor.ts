export class Professor {
    id: number;
    nome: string;
    cpf: string;
    dataNascimento: string;
    sexo: string;
    email: string;
  }