import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Professor } from 'src/app/models/professor';
import { ProfessorService } from 'src/app/services/professor.service';

@Component({
  selector: 'app-add-professor',
  templateUrl: './add-professor.component.html',
  styleUrls: ['./add-professor.component.css']
})
export class AddProfessorComponent implements OnInit {

  professor: Professor = new Professor();

  constructor(private _professorService: ProfessorService,private _router: Router) { }

  ngOnInit(): void {
  }

  saveProfessor() {
    this._professorService.postProfessor(this.professor).subscribe(
      data => {
        console.log('response', data);

        this._router.navigateByUrl("/");
      }
    )
  }
}