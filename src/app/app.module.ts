import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { AddProfessorComponent } from './components/add-professor/add-professor.component';


const routers: Routes = [
  {path: 'addprofessor', component: AddProfessorComponent},
  {path: '', redirectTo: '/addprofessor', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    AddProfessorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routers)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }