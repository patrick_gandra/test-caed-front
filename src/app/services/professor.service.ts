import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { Professor } from "../models/professor";

@Injectable({
  providedIn: "root",
})
export class ProfessorService {
  private getUrl: string = "http://localhost:9292/caed/professor";

  constructor(private _httpClient: HttpClient) {}

  postProfessor(professor: Professor): Observable<Professor> {
    return this._httpClient.post<Professor>(this.getUrl, professor);
  }
}